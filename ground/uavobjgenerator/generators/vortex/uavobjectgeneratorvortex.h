/**
 ******************************************************************************
 *
 * @file       uavobjectgeneratorvortex.h
 * @author     iDrone, http://www.idrone.com Copyright (C) 2016.
 * @brief      produce vortex library code for uavobjects
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef UAVOBJECTGENERATORVORTEX_H
#define UAVOBJECTGENERATORVORTEX_H

#define VORTEX_CODE_DIR "vortex/uavobjects"

#include "../generator_common.h"

class UAVObjectGeneratorVortex {
public:
    bool generate(UAVObjectParser *gen, QString templatepath, QString outputpath);
    QStringList fieldTypeStrC;
    QString vortexCodeTemplate, vortexIncludeTemplate, vortexCollectionTemplate, vortexMakeTemplate;
    QDir vortexCodePath;
    QDir vortexOutputPath;

private:
    bool process_object(ObjectInfo *info);
};

#endif
