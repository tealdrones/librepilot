
// PIOS_INCLUDE_INERTIAL_SENSE should be defined in the firmware makefile, i.e. flight/targets/boards/revolution/firmware (USE_INERTIAL_SENSE = YES)

#if PIOS_INCLUDE_INERTIAL_SENSE

#include "openpilot.h"

void processSensorsInertialSense(void);
void processErrorInertialSense(void);
void inertialSenseSensorsUpdate(void);

#endif