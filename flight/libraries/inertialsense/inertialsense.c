#if PIOS_INCLUDE_INERTIAL_SENSE

#include <inertialsense.h>
#include <com_manager.h>
#include <data_sets.h>
#include <magsensor.h>
#include <accelsensor.h>
#include <gyrosensor.h>
#include <barosensor.h>
#include <stateestimation.h>
#include <attitudestate.h>
#include <positionstate.h>
#include <velocitystate.h>
#include <homelocation.h>
#include <flightstatus.h>
#include <attitudesettings.h>
#include <revocalibration.h>
#include <auxmagsettings.h>
#include <auxmagsensor.h>
#include <auxmagsupport.h>
#include <accelgyrosettings.h>
#include <revosettings.h>
#include <mathmisc.h>
#include <taskinfo.h>
#include <pios_config.h>
#include <pios_math.h>
#include <pios_constants.h>
#include <CoordinateConversions.h>
#include <pios_board_info.h>
#include <string.h>

#define UINS_TICK_MILLISECONDS 6

#define FREQUENCY_INS 100 // hz
#define PERIOD_INS (1000 / FREQUENCY_INS)
#define TICK_TIMEOUT_INS (PERIOD_INS * 20)
static uint16_t uINSTickCounterINS = 0;

#define FREQUENCY_IMU 100 // hz
#define PERIOD_IMU (1000 / FREQUENCY_IMU)
#define TICK_TIMEOUT_IMU (PERIOD_IMU * 20)
static uint16_t uINSTickCounterIMU = 0;

#define FREQUENCY_GPS 5 // hz
#define PERIOD_GPS (1000 / FREQUENCY_GPS)
#define TICK_TIMEOUT_GPS (PERIOD_GPS * 20)
static uint16_t uINSTickCounterGPS = 0;

// sensor data
static AccelSensorData s_accelSensorData;
static GyroSensorData s_gyroSensorData;
static MagSensorData s_magSensorData;
static BaroSensorData s_baroData;

#ifdef PIOS_GPS_SETS_HOMELOCATION

#include <GPS.h>
#include <gpssettings.h>

#endif

static int writeInertialSensePacket(CMHANDLE cmHandle, int pHandle, buffer_t* packet)
{
	// Suppress compiler warnings
	(void)pHandle;
	(void)cmHandle;

	return PIOS_COM_SendBuffer(PIOS_COM_INERTIAL_SENSE, packet->buf, packet->size);
}

static int readInertialSensePacket(CMHANDLE cmHandle, int pHandle, unsigned char* buf, int len)
{
	// Suppress compiler warnings
	(void)pHandle;
	(void)cmHandle;

	return PIOS_COM_ReceiveBuffer(PIOS_COM_INERTIAL_SENSE, buf, len, UINS_TICK_MILLISECONDS);
}

static void requestDataFromuINS()
{
	// Turn off any existing broadcast messages (on all serial ports)
	sendComManager(0, PID_STOP_ALL_BROADCASTS, 0, 0, 0);

	// request uINS system sensors (sys_sensors_t)
	getDataComManager(0, DID_SYS_SENSORS, 0, 0, PERIOD_IMU);

	// request uINS GPS data (gps_t)
	getDataComManager(0, DID_GPS, 0, 0, PERIOD_GPS);

	// not enough bandwidth for this packet...
	// request uINS GPS RSSI Data (gps_rssi_t)
	// getDataComManager(0, DID_GPS_RSSI, 0, 0, PERIOD_GPS);

	if (!StateEstimationEnabled())
	{
		// request uINS INS data (ins_2_t)
		getDataComManager(0, DID_INS_2, 0, 0, PERIOD_INS);
	}
}

#define DEG2RAD_EARTH_RADIUS_F		111120.0f					// = DEG2RAD * earth_radius_in_meters
#define C_DEG2RAD_F					0.017453292519943295769236907684886f
#define _DEG2RAD					C_DEG2RAD_F
#define UNWRAP_DEG_F(x)				{while( (x) > (180.0f) ) (x) -= (360.0f);    while( (x) < (-180.0f) ) (x) += (360.0f);}	    // unwrap to +- 180

#define _SIN        sinf
#define _COS        cosf
#define _TAN        tanf
#define _ASIN       asinf
#define _ACOS       acosf
#define _ATAN2      atan2f

typedef double      Vector3d[3];    // V = | 0 1 2 |
typedef float       f_t;
typedef f_t         Vector3[3];     // V = | 0 1 2 |

									/*
									*  Find NED (north, east, down) from LLAref to LLA (WGS-84 standard)
									*
									*  lla[0] = latitude (decimal degree)
									*  lla[1] = longitude (decimal degree)
									*  lla[2] = msl altitude (m)
									*/
void lla2ned_d(Vector3d llaRef, Vector3d lla, Vector3 result)
{
	Vector3 deltaLLA;
	deltaLLA[0] = (f_t)(lla[0] - llaRef[0]);
	deltaLLA[1] = (f_t)(lla[1] - llaRef[1]);
	deltaLLA[2] = (f_t)(lla[2] - llaRef[2]);

	// Handle longitude wrapping 
	UNWRAP_DEG_F(deltaLLA[1]);

	// Find NED
	result[0] = deltaLLA[0] * DEG2RAD_EARTH_RADIUS_F;
	result[1] = deltaLLA[1] * DEG2RAD_EARTH_RADIUS_F * _COS(((f_t)llaRef[0]) * _DEG2RAD);
	result[2] = -deltaLLA[2];
}

static void handleInertialSenseINS(ins_2_t* ins)
{
	// if using librepilot state estimation, ignore this method
	if (StateEstimationEnabled())
	{
		// turn off ins data as we don't need it - if state estimation is enabled later, this will get turned back on elsewhere
		disableDataComManager(0, DID_INS_2);
		return;
	}

	AlarmsClear(SYSTEMALARMS_ALARM_ATTITUDE);
	AlarmsClear(SYSTEMALARMS_ALARM_STABILIZATION);

	uINSTickCounterINS = 0;
	AttitudeStateData a;
	AttitudeStateGet(&a);
	a.q1 = ins->q[0];
	a.q2 = ins->q[1];
	a.q3 = ins->q[2];
	a.q4 = ins->q[3];
	Quaternion2RPY(&a.q1, &a.Roll);
	AttitudeStateSet(&a);
	VelocityStateData v;
	VelocityStateGet(&v);
	// Rotate body velocities into NED frame
	float Rot[3][3];
	Quaternion2R(ins->q, Rot);
	rot_mult(Rot, ins->uvw, &v.North);
	VelocityStateSet(&v);

	PositionStateData p;
	PositionStateGet(&p);
	HomeLocationData home;
	HomeLocationGet(&home);

	double homeLla[3] = { home.Latitude, home.Longitude, home.Altitude };
	lla2ned_d(homeLla, ins->lla, &p.North);
	PositionStateSet(&p);
}

// taken from http://www.leapsecond.com/tools/gpsdate.c

static uint32_t dateToMjd(int32_t year, int32_t month, int32_t day)
{
	return
		367 * year
		- 7 * (year + (month + 9) / 12) / 4
		- 3 * ((year + (month - 9) / 7) / 100 + 1) / 4
		+ 275 * month / 9
		+ day
		+ 1721028
		- 2400000;
}

static uint32_t gpsToMjd(int32_t gpsCycle, int32_t gpsWeek, int32_t gpsSeconds)
{
	uint32_t gpsDays = ((gpsCycle * 1024) + gpsWeek) * 7 + (gpsSeconds / 86400);
	return dateToMjd(1980, 1, 6) + gpsDays;
}

static void mjdToDate(int32_t mjd, int32_t* year, int32_t* month, int32_t* day)
{
	long j, c, y, m;

	j = mjd + 2400001 + 68569;
	c = 4 * j / 146097;
	j = j - (146097 * c + 3) / 4;
	y = 4000 * (j + 1) / 1461001;
	j = j - 1461 * y / 4 + 31;
	m = 80 * j / 2447;
	*day = j - 2447 * m / 80;
	j = m / 11;
	*month = m + 2 - (12 * j);
	*year = 100 * (c - 49) + y + j;
}

static void handleInertialSenseGPS(gps_t* gps)
{

#define GPS_HOMELOCATION_SET_DELAY 5000

	static portTickType homelocationSetDelay = 0;

	uint32_t comManagerErrorCount = comManagerGetCommunicationErrorCount();
	sendDataComManager(0, 39, &comManagerErrorCount, 4, 0); // 39 = DID_DEBUG_ARRAY

	GPSTimeData GpsTime;
	int32_t mjd = gpsToMjd(0, gps->pos.week, gps->pos.timeOfWeekMs / 1000);
	int32_t year, month, day;
	mjdToDate(mjd, &year, &month, &day);
	GpsTime.Year = year;
	GpsTime.Month = month;
	GpsTime.Day = day;
	GpsTime.Hour = 1;
	GpsTime.Minute = 1;
	GpsTime.Second = 1;
	GPSTimeSet(&GpsTime);

	GPSPositionSensorData gpsPosition;
	GPSPositionSensorGet(&gpsPosition);
	gpsPosition.Latitude = gps->pos.lla[0];
	gpsPosition.Longitude = gps->pos.lla[1];
	gpsPosition.Altitude = gps->pos.lla[2];
	gpsPosition.GeoidSeparation = (gpsPosition.Altitude - gps->pos.hMSL);
	gpsPosition.Groundspeed = gps->vel.s2D;
	gpsPosition.Heading = gps->vel.course;
	gpsPosition.Satellites = gps->pos.status & GPS_STATUS_NUM_SATS_USED_MASK;
	gpsPosition.PDOP = gps->pos.pDop;
	gpsPosition.HDOP = gps->pos.hAcc;
	gpsPosition.VDOP = gps->pos.vAcc;
	GPSPositionSensorSet(&gpsPosition);

	GPSVelocitySensorData gpsVelocity;
	GPSVelocitySensorGet(&gpsVelocity);
	gpsVelocity.North = gps->vel.ned[0];
	gpsVelocity.East = gps->vel.ned[1];
	gpsVelocity.Down = gps->vel.ned[2];
	GPSVelocitySensorSet(&gpsVelocity);
	uint8_t status;
	GPSPositionSensorStatusGet(&status);
	switch (gps->pos.status & GPS_STATUS_FIX_TYPE_MASK)
	{
	default:							status = GPSPOSITIONSENSOR_STATUS_NOFIX;	break;
	case GPS_STATUS_FIX_TYPE_2D_FIX:	status = GPSPOSITIONSENSOR_STATUS_FIX2D;	break;
	case GPS_STATUS_FIX_TYPE_3D_FIX:	status = GPSPOSITIONSENSOR_STATUS_FIX3D;	break;
	}
	GPSPositionSensorStatusSet(&status);
	GPSSettingsData gpsSettings;
	GPSSettingsGet(&gpsSettings);
	if ((gpsPosition.PDOP < gpsSettings.MaxPDOP) &&
		(gpsPosition.Satellites >= gpsSettings.MinSatellites) &&
		(gpsPosition.Status == GPSPOSITIONSENSOR_STATUS_FIX3D) &&
		(gpsPosition.Latitude != 0 || gpsPosition.Longitude != 0)) {
		HomeLocationData home;
		HomeLocationGet(&home);
		uINSTickCounterGPS = 0;
		AlarmsClear(SYSTEMALARMS_ALARM_GPS);

#ifdef PIOS_GPS_SETS_HOMELOCATION

		if (home.Set == HOMELOCATION_SET_FALSE) {
			if (homelocationSetDelay == 0) {
				homelocationSetDelay = xTaskGetTickCount();
			}
			if (xTaskGetTickCount() - homelocationSetDelay > GPS_HOMELOCATION_SET_DELAY) {
				setHomeLocation(&gpsPosition);
				homelocationSetDelay = 0;
			}
		}
		else {
			homelocationSetDelay = 0;
		}

#endif

	}
}

static void handleInertialSenseGPSRSSI(gps_rssi_t* rssi)
{
	uint8_t chan;
	GPSSatellitesData svdata;
	svdata.SatsInView = 0;

	// First, use slots for SVs actually being received
	for (chan = 0; chan < rssi->numSats; chan++) {
		if (svdata.SatsInView < GPSSATELLITES_PRN_NUMELEM && rssi->info[chan].cno > 0) {
			svdata.Azimuth[svdata.SatsInView] = 0;
			svdata.Elevation[svdata.SatsInView] = 0;
			svdata.PRN[svdata.SatsInView] = rssi->info[chan].svId;
			svdata.SNR[svdata.SatsInView] = rssi->info[chan].cno;
			svdata.SatsInView++;
		}
	}

	// fill remaining slots (if any)
	for (chan = svdata.SatsInView; chan < GPSSATELLITES_PRN_NUMELEM; chan++) {
		svdata.Azimuth[chan] = 0;
		svdata.Elevation[chan] = 0;
		svdata.PRN[chan] = 0;
		svdata.SNR[chan] = 0;
	}

	GPSSatellitesSet(&svdata);
}

static void handleInertialSenseIMU(sys_sensors_t* sysSensors)
{
	AlarmsClear(SYSTEMALARMS_ALARM_MAGNETOMETER);
	AlarmsClear(SYSTEMALARMS_ALARM_SENSORS);

	// if using librepilot state estimation, clear attitude and stabilization alarms
	if (StateEstimationEnabled())
	{
		AlarmsClear(SYSTEMALARMS_ALARM_ATTITUDE);
		AlarmsClear(SYSTEMALARMS_ALARM_STABILIZATION);
	}

	uINSTickCounterIMU = 0;

	s_accelSensorData = (AccelSensorData){ sysSensors->acc[0], sysSensors->acc[1], sysSensors->acc[2], sysSensors->temp };
	s_gyroSensorData = (GyroSensorData){ RAD2DEG(sysSensors->pqr[0]), RAD2DEG(sysSensors->pqr[1]), RAD2DEG(sysSensors->pqr[2]), sysSensors->temp, (uint32_t)(sysSensors->time * 1000.0d) };
	s_magSensorData = (MagSensorData){ sysSensors->mag[0], sysSensors->mag[1], sysSensors->mag[2], sysSensors->temp };
	s_baroData = (BaroSensorData){ sysSensors->mslBar, sysSensors->temp, sysSensors->bar };
}

static void processInertialSensePacket(CMHANDLE cmHandle, int pHandle, p_data_t* data)
{
	(void)pHandle;
	(void)cmHandle;

	switch (data->hdr.id)
	{
	case DID_SYS_SENSORS:
		handleInertialSenseIMU((sys_sensors_t*)data->buf);
		break;

	case DID_GPS:
		handleInertialSenseGPS((gps_t*)data->buf);
		break;

	case DID_GPS_RSSI:
		handleInertialSenseGPSRSSI((gps_rssi_t*)data->buf);
		break;

	case DID_INS_2:
		handleInertialSenseINS((ins_2_t*)data->buf);
		break;
	}
}

void processSensorsInertialSense(void)
{
	static char isUInsInitialized = 0;

	if (!isUInsInitialized)
	{
		// initialize Inertial Sense
		isUInsInitialized = 1;

		// 230400 is the highest configurable baud rate in the gcs software. 460800 has a lot of packet errors.
		PIOS_COM_ChangeBaud(PIOS_COM_INERTIAL_SENSE, 230400);

		// setup com manager
		initComManager(1, 0, UINS_TICK_MILLISECONDS, 0, readInertialSensePacket, writeInertialSensePacket, 0, processInertialSensePacket, 0, 0);

		// request data immediately and assume the uINS is already connected
		requestDataFromuINS();

		// Set debug
		// #define DID_DEBUG_ARRAY			39			// (39 debug_array_t)
		// 		uint32_t i = sensor_period_ticks;
		// 		sendDataComManager(0, DID_DEBUG_ARRAY, &i, 4, 0);

	}

	// increment error counters - these are reset to 0 when valid data from the uINS is received
	if (!StateEstimationEnabled())
	{
		// if using uINS state estimation, increment ins counter
		uINSTickCounterINS += UINS_TICK_MILLISECONDS;
	}

	// increment imu and gps counters
	uINSTickCounterIMU += UINS_TICK_MILLISECONDS;
	uINSTickCounterGPS += UINS_TICK_MILLISECONDS;

	stepComManager();

	// serial port function prototypes for reference
	// extern int32_t PIOS_COM_SendBuffer(uint32_t com_id, const uint8_t *buffer, uint16_t len);
	// extern uint16_t PIOS_COM_ReceiveBuffer(uint32_t com_id, uint8_t *buf, uint16_t buf_len, uint32_t timeout_ms);
	// uint16_t cnt = PIOS_COM_ReceiveBuffer(PIOS_COM_INERTIAL_SENSE, buffer, sizeof(buffer) / sizeof(buffer[0]), 20);
}

void processErrorInertialSense(void)
{
	// check the uINS magnetometer and sensors
	if (uINSTickCounterIMU > TICK_TIMEOUT_IMU)
	{
		AlarmsSet(SYSTEMALARMS_ALARM_MAGNETOMETER, SYSTEMALARMS_ALARM_CRITICAL);
		AlarmsSet(SYSTEMALARMS_ALARM_SENSORS, SYSTEMALARMS_ALARM_CRITICAL);

		// if using librepilot state estimation, set attitude and stabilization alarms
		if (StateEstimationEnabled())
		{
			AlarmsSet(SYSTEMALARMS_ALARM_ATTITUDE, SYSTEMALARMS_ALARM_CRITICAL);
			AlarmsSet(SYSTEMALARMS_ALARM_STABILIZATION, SYSTEMALARMS_ALARM_CRITICAL);
		}

		// request uINS system sensors (sys_sensors_t)
		getDataComManager(0, DID_SYS_SENSORS, 0, 0, PERIOD_IMU);
	}

	// if using uINS state estimation, check the ins data timer
	if (!StateEstimationEnabled() && uINSTickCounterINS > TICK_TIMEOUT_INS)
	{
		AlarmsSet(SYSTEMALARMS_ALARM_ATTITUDE, SYSTEMALARMS_ALARM_CRITICAL);
		AlarmsSet(SYSTEMALARMS_ALARM_STABILIZATION, SYSTEMALARMS_ALARM_CRITICAL);

		// request uINS INS data (ins_2_t)
		getDataComManager(0, DID_INS_2, 0, 0, PERIOD_INS);
	}

	// check for gps timeout
	if (uINSTickCounterGPS > TICK_TIMEOUT_GPS)
	{
		AlarmsSet(SYSTEMALARMS_ALARM_GPS, SYSTEMALARMS_ALARM_CRITICAL);

		// request uINS GPS data (gps_t)
		getDataComManager(0, DID_GPS, 0, 0, PERIOD_GPS);

		// request uINS GPS RSSI Data (gps_rssi_t)
		// getDataComManager(0, DID_GPS_RSSI, 0, 0, PERIOD_GPS);
	}
}

void inertialSenseSensorsUpdate(void)
{
	AccelSensorSet(&s_accelSensorData);
	GyroSensorSet(&s_gyroSensorData);
	MagSensorSet(&s_magSensorData);
	BaroSensorSet(&s_baroData);
}

#endif