Librepilot + uINS 2 Instructions
-------------------------------------
1] Setup the uINS using the Inertial Sense Evaluation Tool
	1a] Set ser0baudrate to 230400 under Data Sets / DID_FLASH_CONFIG.
	1b] Set cBrdConfig to 1 if using the uINS evaluation board, otherwise set it to 0.
2] Compile librepilot from https://bitbucket.org/inertialsense/librepilot
	2a] Ensure you are using revolution hardware, as this is the only hardware supporting uINS integration.
	2b] See the revolution Makefile at ./librepilot/flight/targets/boards/revolution/firmware/Makefile. Determine if any modifications are needed, such as removing sensors, etc.
	2c] Compile gcs software and firmware
	2d] Put the new firmware file for the revolution on the revolution hardware using gcs software, firmware tab
3] Open librepilot gcs software
	3a] Configure either the main or flexiport to be "InsertialSense" under Configuration / Hardware
	3b] To use the uINS state estimation, set System / Settings / RevoSettings / Fusion Algorithm to None.
	3c] To use the librepilot state estimatino, set System / Settings / RevoSettings / Fusion Algorithm to something besides None.
	3d] Make sure to click the Save and Send button to get the new settings to the librepilot device.
	3e] Reboot the librepilot device.
4] Connect a cable between the librepilot flexi or main port and uINS hardware if needed.
5] At this point you should be up and running.